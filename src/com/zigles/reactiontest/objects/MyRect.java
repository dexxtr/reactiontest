package com.zigles.reactiontest.objects;

import android.graphics.RectF;

/**
 * Class represents gameobjects (rectangles)
 */
public class MyRect extends GameObject{

	public MyRect(int color) {
		super(color);
	}
	
	/** checking intersections of objects */
	public boolean intersects(RectF r) {
		return rect.intersect(r);
	}
}
