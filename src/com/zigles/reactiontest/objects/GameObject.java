package com.zigles.reactiontest.objects;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;


/**
 * Class represents gameobjects (rectangles)
 */
public abstract class GameObject {

	/** GameObject - rectangle */
	protected RectF rect;
	private Paint rectPaint;
	private float mHeight;
	private float mWidth;
	/** Contain coordinates of the rectangle */
	protected float x;
	protected float y;

	/**
	 * 
	 * @param color rectangle's color
	 * @param width
	 * @param height
	 */
	public GameObject(int color) {
		rect = new RectF();
		rectPaint = new Paint();
		rectPaint.setColor(color);
	}

	/** Sets new coordinates (moves object) */
	public void update() {
		rect.set(x, y, x + mWidth, y + mHeight);
	}

	public void setSize(float width, float height) {
		mWidth = width;
		mHeight = height;
	}

	/**
	 * Draws object on canvas
	 * 
	 * @param canvas
	 */
	public void draw(Canvas canvas) {
		canvas.drawRect(rect, rectPaint);
	}

	/** Sets center of the object by X */
	public void setCenterX(float value) {
		x = value - mHeight / 2;
	}

	/** Sets center of the object by Y */
	public void setCenterY(float value) {
		y = value - mWidth / 2;
	}

	public RectF getRect() {
		return rect;
	}

	public float getLeft() {
		return x;
	}

	public float getRight() {
		return x + mWidth;
	}

	public float getTop() {
		return y;
	}

	public float getBottom() {
		return y + mHeight;
	}
}
