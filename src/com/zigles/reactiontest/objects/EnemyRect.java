package com.zigles.reactiontest.objects;

public class EnemyRect extends GameObject{

	public EnemyRect(int color) {
		super(color);
	}

	public void setX(float X) {
		x = X;
	}

	public void setY(float Y) {
		y = Y;
	}

	public float getX() {
		return x;
	}

	public float getY() {
		return y;
	}
}
