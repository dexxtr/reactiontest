package com.zigles.reactiontest;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.json.JSONObject;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

public class HttpClient {

	public static JSONObject SendHttpPost(String URL, JSONObject jsonObjSend) {

		try {
			DefaultHttpClient httpclient = new DefaultHttpClient();
			HttpConnectionParams.setConnectionTimeout(httpclient.getParams(),
					10000);
			HttpPost httpPostRequest = new HttpPost(URL);

			StringEntity se;
			se = new StringEntity(jsonObjSend.toString());
			httpPostRequest.setEntity(se);
			httpPostRequest.setHeader("Accept", "application/json");
			httpPostRequest.setHeader("Content-type", "application/json");

			HttpResponse response = (HttpResponse) httpclient
					.execute(httpPostRequest);

			HttpEntity entity = response.getEntity();
			InputStream is = entity.getContent();
			String resultString = "";
			try {
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(is, "utf-8"), 8);
				StringBuilder sb = new StringBuilder();
				String line = null;
				while ((line = reader.readLine()) != null) {
					sb.append(line + "\n");
				}
				is.close();
				resultString = sb.toString();
			} catch (Exception e) {
				e.printStackTrace();
			}
			//System.out.println(resultString);
			JSONObject jsonObjRecv = new JSONObject(resultString);
			Log.i("HttpClient", "<jsonobject>\n" + jsonObjRecv.toString()
					+ "\n</jsonobject>");
			httpclient.clearRequestInterceptors();

			return jsonObjRecv;

		} catch (Exception ex) {
			//ex.printStackTrace();
			Log.e("Http Client Exception", ex.getMessage());
		}
		return null;
	}

	public static boolean checkConnection(Object systemService) throws IOException {

		ConnectivityManager cm = (ConnectivityManager) systemService;
		if (cm.getActiveNetworkInfo().isConnectedOrConnecting()) {
			URL url = new URL("http://www.google.com");
			HttpURLConnection urlc = (HttpURLConnection) url.openConnection();
			urlc.setRequestProperty("User-Agent", "Android Game");
			urlc.setRequestProperty("Connection", "close");
			urlc.setConnectTimeout(4000);
			urlc.connect();

			if (urlc.getResponseCode() == 200) {
				urlc.disconnect();
				Log.i("HttpClient", "connected");
				return true;
			}
			urlc.disconnect();
		}
		Log.i("HttpClient", "no connection");
		return false;
	}
	
	public static boolean isOnline(Context context) {
		try {
			ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
			if (cm.getActiveNetworkInfo().isConnectedOrConnecting()) {
				URL url = new URL("http://www.google.com/");
				HttpURLConnection urlc = (HttpURLConnection) url.openConnection();
				urlc.setRequestProperty("User-Agent", "Android Game");
				urlc.setRequestProperty("Connection", "close");
				urlc.setConnectTimeout(1000); // mTimeout is in seconds
				 
				urlc.connect();
			 
				if (urlc.getResponseCode() == 200) {
					return true;
				} else {
					return false;
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return false;
	}
	
	public static boolean isNetworkAvailable(Context context) {
		ConnectivityManager connectivity = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		if (connectivity == null) {
			Log.w("INTERNET", "couldn't get connectivity manager");
		} else {
			NetworkInfo[] info = connectivity.getAllNetworkInfo();
			if (info != null) {
				for (int i = 0; i < info.length; i++) {
					if (info[i].getState() == NetworkInfo.State.CONNECTED) {
						return true;
					}
				}
			}
		}
		return false;
	}
}
