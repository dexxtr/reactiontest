package com.zigles.reactiontest;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class ScoreBoard extends ListActivity {

	private ProgressDialog m_ProgressDialog = null;
	private ArrayList<Player> m_players = null;
	private OrderAdapter m_adapter;
	private Runnable viewOrders;
	private Context context;
	private Thread thread;
	private String topPlayersURL;
	private String currentPlayerURL;
	private Object systemService;
	private Runnable returnRes = new Runnable() {
		@Override
		public void run() {
			if (m_players != null && m_players.size() > 0) {
				m_adapter.notifyDataSetChanged();
				for (int i = 0; i < m_players.size(); i++)
					m_adapter.add(m_players.get(i));

			} else {
				Toast.makeText(context, "Connection error or you are not registered.", Toast.LENGTH_SHORT)
						.show();
				finish();
			}
			try {
				m_ProgressDialog.dismiss();
			} catch (IllegalArgumentException e) {
				Log.e("IllegalArgumentException", e.getMessage());
			}
			m_adapter.notifyDataSetChanged();
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.scoreboard);

		Bundle bundle = this.getIntent().getExtras();
		String period = bundle.getString("period");
		if (period.equals("day")) {
			topPlayersURL = "http://reactiontest.zigles.com/score_table/get_top_players_daily/";
			currentPlayerURL = "http://reactiontest.zigles.com/score_table/get_player_rating_by_daily/";
		} else if (period.equals("week")) {
			topPlayersURL = "http://reactiontest.zigles.com/score_table/get_top_players_week/";
			currentPlayerURL = "http://reactiontest.zigles.com/score_table/get_player_rating_by_weekly/";
		} else {
			topPlayersURL = "http://reactiontest.zigles.com/score_table/get_top_players_month/";
			currentPlayerURL = "http://reactiontest.zigles.com/score_table/get_player_rating_by_month/";
		}

		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);

		systemService = this.getSystemService(Context.CONNECTIVITY_SERVICE);

		context = this.getBaseContext();
		m_players = new ArrayList<Player>();

		this.m_adapter = new OrderAdapter(this, R.layout.row, m_players);
		setListAdapter(this.m_adapter);

		viewOrders = new Runnable() {
			@Override
			public void run() {
				getPlayers();
			}
		};

		thread = new Thread(null, viewOrders, "MagentoBackground");
		thread.start();

		m_ProgressDialog = ProgressDialog.show(ScoreBoard.this,
				"Please wait...", "Retrieving data ...", true);
	}

	private void getPlayers() {
		m_players = new ArrayList<Player>();
		try {
			if (HttpClient.checkConnection(systemService)) {
				int count = 0;
				JSONObject jsonObjSendTop = new JSONObject();
				try {
					JSONObject dataTop = new JSONObject();
					dataTop.put("api_login_id", "12389");
					jsonObjSendTop.put("data", dataTop);
				} catch (JSONException e) {
					e.printStackTrace();
				}

				JSONObject jsonTopPlayers = HttpClient.SendHttpPost(
						topPlayersURL, jsonObjSendTop);

				if (jsonTopPlayers != null) {
					try {
						count = jsonTopPlayers.getInt("count");
						JSONArray players = jsonTopPlayers
								.getJSONArray("players");
						for (int i = 0; i < players.length(); i++) {
							JSONObject player = players.getJSONObject(i);
							m_players.add(new Player(player.getString("name"),
									player.getString("time"), player
											.getString("result_created"),
									R.drawable.player, i + 1));
						}
					} catch (JSONException e) {
						e.printStackTrace();
					}
				}

				String nameCurrent = GameScreen.getPlayername();
				if (!nameCurrent.equals("0")) {
					JSONObject jsonObjSendCurrent = new JSONObject();
					try {
						JSONObject dataCurrent = new JSONObject();
						dataCurrent.put("api_login_id", "12389");
						dataCurrent.put("player_name", nameCurrent);
						jsonObjSendCurrent.put("data", dataCurrent);
					} catch (JSONException e) {
						e.printStackTrace();
					}

					JSONObject jsonCurrentPlayer = HttpClient.SendHttpPost(
							currentPlayerURL, jsonObjSendCurrent);

					if (jsonCurrentPlayer != null) {
						try {
							String strpos = jsonCurrentPlayer
									.getString("rating");
							if (!strpos.equals("null")) {
								String date = jsonCurrentPlayer
										.getString("result_created");
								String time = jsonCurrentPlayer
										.getString("time");
								m_players.add(0, new Player(nameCurrent
										+ " (You) of " + count + " players.",
										time, date, R.drawable.main_player,
										Integer.valueOf(strpos)));
							} /*
							else {
								if (currentPlayerURL.equals("http://reactiontest.zigles.com/score_table/get_player_rating_by_month/")){
									
									GameScreen.setSavedBestResult(0);
									SharedPreferences preferences = this.getSharedPreferences(
											"Prefs13", MODE_WORLD_READABLE);
									SharedPreferences.Editor editor = preferences.edit();
									editor.putInt("success", 0);
									editor.putFloat("BestResult", 0);
									editor.commit();
									
									//Toast.makeText(this, "Your best result was zeroed due to it obsoleteness.", Toast.LENGTH_SHORT).show();			
								}
							}*/
						} catch (JSONException e) {
							e.printStackTrace();
						}
					}
				}
			}
		} catch (UnknownHostException ex) {
			Log.e("UnknownHostException", ex.getMessage());
		} catch (NullPointerException e) {
			Log.e("NullPointerException", "");
		} catch (IOException e) {
			Log.e("IOException", e.getMessage());
		} catch (Exception e) {
			Log.e("Exception", e.getMessage());
		}

		runOnUiThread(returnRes);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		boolean retry = true;
		while (retry) {
			try {
				thread.join();
				retry = false;
			} catch (Exception e) {
				Log.e("Exception", e.getMessage());
			}
		}
	}

	private class OrderAdapter extends ArrayAdapter<Player> {

		private LayoutInflater mInflate;
		private ArrayList<Player> items;

		public OrderAdapter(Context context, int textViewResourceId,
				ArrayList<Player> items) {
			super(context, textViewResourceId, items);
			this.items = items;
			mInflate = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			ViewHolder holder;
			if (convertView == null) {
				convertView = mInflate.inflate(R.layout.row, null);
				holder = new ViewHolder();
				holder.toptext = (TextView) convertView
						.findViewById(R.id.toptext);
				holder.bottomtext = (TextView) convertView
						.findViewById(R.id.bottomtext);
				holder.icon = (ImageView) convertView.findViewById(R.id.icon);
				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}
			Player p = items.get(position);
			holder.toptext.setText("#" + p.getPosition() + " "
					+ p.getPlayerName());
			holder.bottomtext.setText("Score: " + p.getPlayerTime() + " Date: "
					+ p.getDate());
			holder.icon.setImageDrawable(this.getContext().getResources()
					.getDrawable(p.getIconId()));

			return convertView;
		}
	}

	static class ViewHolder {
		TextView toptext;
		TextView bottomtext;
		ImageView icon;
	}

	private class Player {
		private String playerName;
		private String playerTime;
		private String date;
		private int iconId;
		private int position;

		public Player(String playerName, String playerTime, String date,
				int iconId, int pos) {
			this.playerName = playerName;
			this.playerTime = playerTime;
			this.date = date;
			this.iconId = iconId;
			this.position = pos;
		}

		public int getPosition() {
			return position;
		}

		public String getPlayerName() {
			return playerName;
		}

		public String getPlayerTime() {
			return playerTime;
		}

		public String getDate() {
			return date;
		}

		public int getIconId() {
			return iconId;
		}
	}
}
