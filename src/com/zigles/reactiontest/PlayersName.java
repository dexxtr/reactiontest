package com.zigles.reactiontest;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class PlayersName extends Activity implements OnClickListener {
	private EditText editLogin;
	private Object systemService;	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.playersname);
		systemService = this.getSystemService(Context.CONNECTIVITY_SERVICE);
		editLogin = ((EditText) findViewById(R.id.edit));
		Button btnEnter = (Button) findViewById(R.id.buttonEnter);
		btnEnter.setOnClickListener(this);
	}

	private void showErrorToastAndShake(String msg) {
		Animation shake = AnimationUtils.loadAnimation(this, R.anim.shake);
		editLogin.startAnimation(shake);
		Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
		editLogin.setText("");
		editLogin.requestFocus();
	}
	
	public boolean isValid(final String username) {
		Pattern pattern = Pattern.compile("^[a-z0-9_-]{3,15}$");
		Matcher matcher = pattern.matcher(username);
		return matcher.matches();
	}
	
	@Override
	public void onClick(View arg0) {
		String playername = editLogin.getText().toString().toLowerCase();
		if (isValid(playername)) {
			JSONObject jsonObjSend = new JSONObject();
			try {
				JSONObject data = new JSONObject();
				data.put("api_login_id", "12389");
				data.put("player_name", playername);
				data.put("phone_manufacturer", Build.MANUFACTURER);
				data.put("phone_model", Build.MODEL);
				jsonObjSend.put("data", data);
			} catch (JSONException e) {
				Log.e("json creation", e.getMessage());
			}
			try {
				if (HttpClient.checkConnection(systemService)) {
						JSONObject jsonObjRecv = HttpClient
								.SendHttpPost(
										"http://reactiontest.zigles.com/score_table/add_player/",
										jsonObjSend);
						if (jsonObjRecv == null) {
							Toast.makeText(this, "Server doesn't response.",
									Toast.LENGTH_SHORT).show();
							finish();
						} else {
							int result = 0;
							try {
								result = jsonObjRecv.getInt("success");
							} catch (JSONException e) {
								Log.e("json parsing", e.getMessage());
							}
							if (result == 1) {
								SharedPreferences preferences = getSharedPreferences(
										"Prefs13", MODE_WORLD_READABLE);
								SharedPreferences.Editor editor = preferences
										.edit();
								editor.putString("Name", playername);
								editor.commit();
								GameScreen.setPlayername(playername);
								Toast.makeText(this,
										"Hello " + playername + "!",
										Toast.LENGTH_LONG).show();
								finish();
							} else {
								String s = "";
								try {
									s = jsonObjRecv.getString("error");
								} catch (JSONException e) {
									Log.e("json parsing error", e.getMessage());
								}
								if (s.equalsIgnoreCase("invalid player name")) {
									showErrorToastAndShake("Invalid name!");
								} else {
									showErrorToastAndShake("This name is already in use.");
								}
							}
						}
				} else {
					Toast.makeText(this, "No internet connection!",
							Toast.LENGTH_SHORT).show();
					finish();
				}
			} catch (UnknownHostException ex){
				Log.e("connection error", ex.getMessage());
				Toast.makeText(this, "No internet connection!",
						Toast.LENGTH_SHORT).show();
				finish();
			} catch (NullPointerException e) {
				Log.e("NullPointerException", "");
				Toast.makeText(this, "Cannot establish internet connection!",
						Toast.LENGTH_SHORT).show();
				finish();
			}
			catch (IOException e) {
				Log.e("IO error", e.getMessage());
				Toast.makeText(this, "No internet connection!",
						Toast.LENGTH_SHORT).show();
				finish();
			}
		} else {
			showErrorToastAndShake("Invalid name!");
		}
	}
}
