package com.zigles.reactiontest;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.flurry.android.FlurryAgent;

public class LoadScreen extends Activity {
    
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.loadscreen);
		final Intent intent = new Intent(this, StartScreen.class);
		
		final Handler speedHandler = new Handler();
		Runnable runnable = new Runnable() {
			@Override
			public void run() {
				startActivity(intent);
				finish();
			}
		};
		speedHandler.postDelayed(runnable, 1000);
	}
	
	public void onStart() {
 	   super.onStart();
 	   FlurryAgent.onStartSession(this, "D1ZEQSPTRRIVYPZGPJUQ");
 	}
 	
 	public void onStop() {
 	   super.onStop();
 	   FlurryAgent.onEndSession(this);
 	}
}
