package com.zigles.reactiontest;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.Calendar;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings.Secure;
import android.util.Log;
import android.view.KeyEvent;

import com.flurry.android.FlurryAgent;
import com.google.ads.*;

public class GameScreen extends Activity {
	private static float bestResult = 0;
	private static float lastResult = 0;
	private static float todaysBestResult = 0;
	private static String playername = "";
	private ProgressDialog progressDialog;
	private Object systemService;
	private SharedPreferences.Editor editor;
	
	public static String getPlayername() {
		return playername;
	}

	public static float getSavedBestResult() {
		return bestResult;
	}

	public static float getSavedLastResult() {
		return lastResult;
	}

	public static void setPlayername(String playername) {
		GameScreen.playername = playername;
	}

	public static void setSavedBestResult(float bResult) {
		GameScreen.bestResult = bResult;
	}

	public static void setSavedLastResult(float lResult) {
		GameScreen.lastResult = lResult;
	}

	public static float getTodaysBestResult() {
		return todaysBestResult;
	}

	public static void setTodaysBestResult(float todaysBestResult) {
		GameScreen.todaysBestResult = todaysBestResult;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.gamescreen);
		
		AdView adView = (AdView)this.findViewById(R.id.ad2);
	    adView.loadAd(new AdRequest());
	    
		progressDialog = new ProgressDialog(this);
		progressDialog.setMessage("Saving your result. Wait...");
		systemService = this.getSystemService(Context.CONNECTIVITY_SERVICE);
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if ((keyCode == KeyEvent.KEYCODE_BACK)) {
			SharedPreferences preferences = this.getSharedPreferences(
					"Prefs13", MODE_WORLD_READABLE);
			editor = preferences.edit();
			
			float oldBestResult = preferences.getFloat("BestResult", 0);
			if (bestResult > oldBestResult) {
				editor.putFloat("BestResult", bestResult);
				editor.commit();
			}
			
			float oldBestTodaysResult = preferences.getFloat("TodaysBestResult", 0);
			int success = preferences.getInt("success", 0);
			if (todaysBestResult>oldBestTodaysResult || success == 0){
				final Calendar c = Calendar.getInstance();
				final String currentDate = c.get(Calendar.DAY_OF_MONTH)+" "+c.get(Calendar.MONTH)+" "+c.get(Calendar.YEAR); 
				editor.putString("Date", currentDate);
				editor.putFloat("TodaysBestResult", todaysBestResult);
				editor.commit();
				if (!playername.equals("0")) {
					progressDialog.show();
					new Thread(worker).start();
				} else{
					finish();
				}
			} else {
				finish();
			}
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	private Handler handleWorker = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			editor.putInt("success", 0);
			editor.commit();
			progressDialog.hide();
			progressDialog.cancel();
			finish();
		}
	};

	private Runnable worker = new Runnable() {
		@Override
		public void run() {
			try {
				ConnectivityManager cm = (ConnectivityManager) systemService;
				if (cm.getActiveNetworkInfo().isConnectedOrConnecting()) {
					URL url = new URL("http://www.google.com.ua");
					HttpURLConnection urlc = (HttpURLConnection) url
							.openConnection();
					urlc.setRequestProperty("User-Agent", "My Android Game");
					urlc.setRequestProperty("Connection", "close");
					urlc.setConnectTimeout(1000);
					urlc.connect();
					if (urlc.getResponseCode() == 200) {
						JSONObject jsonObjSend = new JSONObject();
						try {
							JSONObject data = new JSONObject();
							data.put("api_login_id", "12389");
							data.put("player_name", playername);
							data.put("phone_manufacturer", Build.MANUFACTURER);
							data.put("phone_model", Build.MODEL);
							data.put("new_time", Float.toString(todaysBestResult));
							data.put("code",
									(int) (todaysBestResult * 17 * playername
											.length()));
							jsonObjSend.put("data", data);
						} catch (JSONException e) {
							e.printStackTrace();
						}
						HttpClient
								.SendHttpPost(
										"http://reactiontest.zigles.com/score_table/publish_result/",
										jsonObjSend);

						handleWorker.post(new Runnable() {
							public void run() {
								editor.putInt("success", 1);
								editor.commit();
								progressDialog.hide();
								progressDialog.cancel();
								finish();
							}
						});

					} else {
						handleWorker.sendMessage(handleWorker.obtainMessage(0));
					}
					urlc.disconnect();
					cm = null;
				} else {
					handleWorker.sendMessage(handleWorker.obtainMessage(0));
				}
			} catch (UnknownHostException ex){
				Log.e("UnknownHostException", ex.getMessage());
				handleWorker.sendMessage(handleWorker.obtainMessage(0));
			} catch (NullPointerException e) {
				Log.e("NullPointerException", "");
				handleWorker.sendMessage(handleWorker.obtainMessage(0));
			} catch (IOException e) {
				handleWorker.sendMessage(handleWorker.obtainMessage(0));
				Log.e("IOException", e.getMessage());
			}
		}
	};

	@Override
	protected void onResume() {
		super.onResume();
		setContentView(R.layout.gamescreen);
	}
	
	public void onStart() {
 	   super.onStart();
 	   FlurryAgent.onStartSession(this, "D1ZEQSPTRRIVYPZGPJUQ");
 	   
 	   String uid = Secure.getString(this.getContentResolver(), Secure.ANDROID_ID);
	   FlurryAgent.setUserId(uid);
	   
	   FlurryAgent.onPageView();
 	}
 	
 	public void onStop() {
 	   super.onStop();
 	   FlurryAgent.onEndSession(this);
 	}
}