package com.zigles.reactiontest;

import android.app.TabActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.WindowManager;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;

public class ScoreBoardTabBar extends TabActivity {
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tabs);
        
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
        
        TabHost tabHost = (TabHost)findViewById(android.R.id.tabhost);

        TabSpec dailyTabSpec = tabHost.newTabSpec("tid1");
        TabSpec weeklyTabSpec = tabHost.newTabSpec("tid2");
        TabSpec monthlyTabSpec = tabHost.newTabSpec("tid3");      

        Intent dailyIntent = new Intent(this, ScoreBoard.class);
		Bundle dailyBundle = new Bundle();
		dailyBundle.putString("period", "day");
		dailyIntent.putExtras(dailyBundle);
		
        Intent weeklyIntent = new Intent(this, ScoreBoard.class);
		Bundle weeklyBundle = new Bundle();
		weeklyBundle.putString("period", "week");
		weeklyIntent.putExtras(weeklyBundle);
		
        Intent monthlyIntent = new Intent(this, ScoreBoard.class);
		Bundle monthlyBundle = new Bundle();
		monthlyBundle.putString("period", "month");
		monthlyIntent.putExtras(monthlyBundle);
        
        dailyTabSpec.setIndicator("Daily", getResources().getDrawable(R.drawable.tab_artists)).setContent(dailyIntent);
        weeklyTabSpec.setIndicator("Weekly", getResources().getDrawable(R.drawable.tab_artists)).setContent(weeklyIntent);
        monthlyTabSpec.setIndicator("Monthly", getResources().getDrawable(R.drawable.tab_artists)).setContent(monthlyIntent);
        
        tabHost.addTab(dailyTabSpec);
        tabHost.addTab(weeklyTabSpec);
        tabHost.addTab(monthlyTabSpec);
    }
}
