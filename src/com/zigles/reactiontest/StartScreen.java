package com.zigles.reactiontest;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Calendar;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.provider.Settings.Secure;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

import com.flurry.android.FlurryAgent;
import com.zigles.android.expll.Launcher;

public class StartScreen extends Activity implements OnClickListener {
	private Method killMethod;
	private ActivityManager activityManager;
	
	public void onStart() {
 	   super.onStart();
 	   FlurryAgent.onStartSession(this, "D1ZEQSPTRRIVYPZGPJUQ");
 	   
 	   String uid = Secure.getString(this.getContentResolver(), Secure.ANDROID_ID);
	   FlurryAgent.setUserId(uid);
	   
	   FlurryAgent.onPageView();
 	}
 	
 	public void onStop() {
 	   super.onStop();
 	   FlurryAgent.onEndSession(this);
 	}
	 	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.start);
		activityManager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
		initializeKillMethod();
		
		// "Start"
		Button startButton = (Button) findViewById(R.id.StartButton);
		startButton.setOnClickListener(this);

		// "How To Play"
		Button pollButton = (Button) findViewById(R.id.PollButton);
		pollButton.setOnClickListener(this);

		// "Credits"
		Button creditsButton = (Button) findViewById(R.id.CreditsButton);
		creditsButton.setOnClickListener(this);

		// "Exit"
		Button exitButton = (Button) findViewById(R.id.ExitButton);
		exitButton.setOnClickListener(this);

		// Score Board
		Button scoreButton = (Button) findViewById(R.id.VoteButton);
		scoreButton.setOnClickListener(this);

		SharedPreferences preferences = this.getSharedPreferences("Prefs13",
				MODE_WORLD_READABLE);
		GameScreen.setSavedBestResult(preferences.getFloat("BestResult", 0));
		GameScreen.setTodaysBestResult(preferences.getFloat("TodaysBestResult", 0));
		
		if(!(GameScreen.getTodaysBestResult()==0)){
			final Calendar c = Calendar.getInstance();
			final String currentDate = c.get(Calendar.DAY_OF_MONTH)+" "+c.get(Calendar.MONTH)+" "+c.get(Calendar.YEAR); 
			final String date = preferences.getString("Date", "null");
			if (!date.equals(currentDate)){
				SharedPreferences.Editor editor = preferences.edit();
				editor.putFloat("TodaysBestResult", 0);
				editor.commit();
				GameScreen.setTodaysBestResult(0);
			}
		}

		String playername = preferences.getString("Name", "0");
		GameScreen.setPlayername(playername);
		
		if (playername.equals("0")) {
			Intent intent = new Intent();
			intent.setClass(this, PlayersName.class);
			startActivity(intent);
		} else {
			Toast.makeText(this, "Hello " + playername + "!", Toast.LENGTH_SHORT)
					.show();
		}

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.StartButton: {
			final Intent intent = new Intent();
			intent.setClass(this, GameScreen.class);
			startActivity(intent);
			break;
		}
		case R.id.PollButton: {
			Launcher exPllLauncher = new Launcher(this.getApplicationContext());
			exPllLauncher.run();
			break;
		}
		case R.id.CreditsButton: {
			Intent intent = new Intent();
			intent.setClass(this, Credits.class);
			startActivity(intent);
			break;
		}
		//Score Board
		case R.id.VoteButton: {
			Intent intent = new Intent();
			intent.setClass(this, ScoreBoardTabBar.class);
			startActivity(intent);
			break;
		}
		case R.id.ExitButton: {
			finish();
			
			if (this.killMethod != null) {
	            try {
	                this.killMethod.invoke(this.activityManager, "com.zigles.reactiontest");
	            } catch (IllegalArgumentException e) {
	                e.printStackTrace();
	            } catch (IllegalAccessException e) {
	                e.printStackTrace();
	            } catch (InvocationTargetException e) {
	                e.printStackTrace();
	            }
	        }
			int pid = android.os.Process.myPid();
	        android.os.Process.killProcess(pid);
	        System.exit(0);
			break;
		}
		default:
			break;
		}
	}

	private void initializeKillMethod() {
		try {
			this.killMethod = ActivityManager.class.getMethod(
					"killBackgroundProcesses", String.class);
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		}

		if (this.killMethod != null) {
			return;
		}

		try {
			this.killMethod = ActivityManager.class.getMethod("restartPackage",
					String.class);
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		}
	}
}
