package com.zigles.reactiontest;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.os.Vibrator;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

/**
 * View
 * 
 */
public class GameView extends SurfaceView implements SurfaceHolder.Callback {

	private final static int START_DRAGGING = 1;
	private final static int STOP_DRAGGING = 0;
	private static int status = -1;
	private GameManager mThread;
	private SurfaceHolder mSurfaceHolder;

	/**
	 * Constructor
	 * 
	 * @param context
	 */
	public GameView(Context context, AttributeSet attrs) {
		super(context, attrs);
		mSurfaceHolder = getHolder();
		mSurfaceHolder.addCallback(this);
		final Vibrator v = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
		mThread = new GameManager(mSurfaceHolder, context, new Handler() {
			@Override
			public void handleMessage(Message m) {
				v.vibrate(300);
			}
		});
		
		setClickable(true);
		setFocusable(true);
	}

	public static void setDraggingStatus(int st){
		status = st;
	}
	
	@Override
	public void surfaceChanged(SurfaceHolder arg0, int format, int width,
			int height) {
		mThread.setObjectsSize(width, height);
		/*
		 * 				getResources().getConfiguration().orientation);
		 */
		mThread.setResults(GameScreen.getSavedBestResult(), GameScreen.getSavedLastResult());
		
	}

	@Override
	public void surfaceCreated(SurfaceHolder arg0) {
		mThread.setRunning(true);
		mThread.start();
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder arg0) {
		boolean retry = true;
		mThread.setRunning(false);
		while (retry) {
			try {
				// Waits for this thread to die
				mThread.join();
				retry = false;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public boolean onTouchEvent(MotionEvent me) {
		if (me.getAction() == MotionEvent.ACTION_DOWN) {
			if (mThread.isMyRectContaining(me.getX(), me.getY()) == true) {
				status = START_DRAGGING;
				if (!mThread.isTimerRunning()) {
					mThread.setAIRunning();
					mThread.startTimer();
				}
			}
		}
		if (me.getAction() == MotionEvent.ACTION_UP) {
			status = STOP_DRAGGING;
		} else if (me.getAction() == MotionEvent.ACTION_MOVE) {
			if (status == START_DRAGGING) {
				mThread.setPos(me.getX(), me.getY());
			}
		}
		return true;
	}
}
