package com.zigles.reactiontest;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Typeface;
import android.os.Handler;
import android.view.SurfaceHolder;

import com.zigles.reactiontest.objects.EnemyRect;
import com.zigles.reactiontest.objects.MyRect;

/**
 * 
 * Manages game objects and game process.
 */
public class GameManager extends Thread {
	private Bitmap mBitmap2;
	private float mScreenHeight;
	private float mScreenWidth;
	private int screenCenterX, screenCenterY;
	private int mScreenWidthRightText, mScreenWidthLeftText;
	private int tmp;
	private float minSpeed;
	private float speedByTime;
	private float cx1, cy1, cx2, cy2, cx3, cy3, cx4, cy4;
	private float lastResult;
	private float startTime;
	private float stopTime;
	private float bestResult;
	private final int[] enemyXdir;
	private final int[] enemyYdir;
	private final float[] speed;
	private final EnemyRect[] enemies;
	private final MyRect myRect;
	private final SurfaceHolder mSurfaceHolder;
	private final Handler mHandler, speedHandler;
	private final Runnable runnable;
	private final Paint mScorePaint;
	private boolean timerIsRunning;
	private boolean mRunning;

	private final ScoreBar lastScoreText;
	private final ScoreBar bestScoreText;
	private double dd;

	/**
	 * Constructor
	 * 
	 * @param surfaceHolder
	 * @param context
	 */
	public GameManager(SurfaceHolder surfaceHolder, Context context,
			Handler handler) {
		mSurfaceHolder = surfaceHolder;
		mHandler = handler;
		mRunning = false;
		timerIsRunning = false;
		bestResult = 0;
		lastResult = 0;
		speedByTime = 1;
		dd = Math.pow(10, 2);

		mScorePaint = new Paint();
		mScorePaint.setColor(Color.BLACK);
		mScorePaint.setTypeface(Typeface.createFromAsset(context.getAssets(),
				"fonts/Mini.ttf"));
		mScorePaint.setStrokeWidth(1);
		mScorePaint.setStyle(Style.FILL);
		mScorePaint.setTextAlign(Paint.Align.CENTER);

		myRect = new MyRect(Color.RED);
		enemies = new EnemyRect[4];
		enemies[0] = new EnemyRect(Color.BLUE);
		enemies[1] = new EnemyRect(Color.BLUE);
		enemies[2] = new EnemyRect(Color.BLUE);
		enemies[3] = new EnemyRect(Color.BLUE);

		enemyXdir = new int[] { rx(), rx(), rx(), rx() };
		enemyYdir = new int[] { rx(), rx(), rx(), rx() };

		speed = new float[] { 0, 0, 0, 0 };

		speedHandler = new Handler();
		runnable = new Runnable() {
			@Override
			public void run() {
				//speedByTime = (float) (speedByTime + 0.35);
				speedHandler.postDelayed(this, 8000);
			}
		};

		lastScoreText = new ScoreBar("Last result: ");
		bestScoreText = new ScoreBar("Today Best result: ");
		
		java.io.InputStream is;
		is = context.getResources().openRawResource(R.drawable.bg);
        mBitmap2 = BitmapFactory.decodeStream(is);
	}

	public void setObjectsSize(int screenWidth, int screenHeight) {
		synchronized (mSurfaceHolder) {
			mScreenWidth = screenWidth;
			mScreenHeight = screenHeight;
			screenCenterX = screenWidth / 2;
			screenCenterY = screenHeight / 2;
			minSpeed = (mScreenWidth * mScreenHeight) / (240 * 320);

			mScreenWidthRightText = (int) (screenCenterX + 57 + minSpeed*3);
			mScreenWidthLeftText = (int) (screenCenterX - 72 - minSpeed*3);

			float x = (float) (mScreenWidth * 0.12);
			float y = (float) (mScreenHeight * 0.12);
			//if (orientation == 1) {
				cx1 = (float) (mScreenWidth * 0.93);
				cy1 = mScreenHeight - y;

				cx2 = mScreenWidth / 6;
				cy2 = mScreenHeight / 6;

				cx3 = mScreenWidth - x * 3;
				cy3 = mScreenHeight / 5;

				cx4 = mScreenWidth / 9;
				cy4 = (float) (mScreenHeight * 0.94);
			/*} else {
				float tmp;
				tmp = x;
				x = y;
				y = tmp;
				cx1 = (float) (mScreenWidth * 0.95);
				cy1 = mScreenHeight - y;

				cx2 = mScreenWidth / 8;
				cy2 = mScreenHeight / 5;

				cx3 = mScreenWidth - x * 3;
				cy3 = mScreenHeight / 4;

				cx4 = mScreenWidth / 12;
				cy4 = (float) (mScreenHeight * 0.9);
			}*/

			myRect.setSize(x + 2, x + 2);
			enemies[0].setSize(x, y);// 3
			enemies[1].setSize((float) (x * 1.3), (float) (y * 1.5));// 1
			enemies[2].setSize((float) (x * 3), (float) (y * 0.4));// 2
			enemies[3].setSize(x * 2, y);// 4

			mScorePaint.setTextSize(7 + minSpeed);
			System.gc();
			initPositions();
		}
	}

	/**
	 * Initializes objects
	 * 
	 * @param screenHeight
	 * @param screenWidth
	 */
	public void initPositions() {
		myRect.setCenterX(screenCenterX);
		myRect.setCenterY(screenCenterY);

		enemies[0].setCenterX(cx1);
		enemies[0].setCenterY(cy1);

		enemies[1].setCenterX(cx2);
		enemies[1].setCenterY(cy2);

		enemies[2].setCenterX(cx3);
		enemies[2].setCenterY(cy3);

		enemies[3].setCenterX(cx4);
		enemies[3].setCenterY(cy4);
		System.gc();
		System.gc();
	}

	public void setPos(float x, float y) {
		synchronized (mSurfaceHolder) {
			myRect.setCenterX(x);
			myRect.setCenterY(y);
		}
	}

	public boolean isMyRectContaining(float x, float y) {
		return myRect.getRect().contains(x, y);
	}

	public void setResults(float bResult, float lResult) {
		synchronized (mSurfaceHolder) {
			bestResult = bResult;
			lastResult = lResult;
			lastScoreText.setValue(lastResult);
			bestScoreText.setValue(GameScreen.getTodaysBestResult());
		}
	}

	/**
	 * Sets the state of the thread
	 * 
	 * @param running
	 */
	public void setRunning(boolean running) {
		mRunning = running;
	}

	public void setAIRunning() {
		synchronized (mSurfaceHolder) {
			speedHandler.postDelayed(runnable, 8000);
			speed[0] = (float) (minSpeed + 1.5);
			speed[1] = minSpeed + 1;
			speed[2] = minSpeed;
			speed[3] = (float) (minSpeed + 0.5);
		}
	}

	public boolean isTimerRunning() {
		return timerIsRunning;
	}

	public void startTimer() {
		synchronized (mSurfaceHolder) {
			startTime = System.nanoTime();
			timerIsRunning = true;
		}
	}

	private void stopTimer() {
		stopTime = System.nanoTime();
		timerIsRunning = false;
	}

	private void setResult() {
		lastResult = (float) (Math.round(((stopTime - startTime) / 1000000000) * dd) / dd);
		if (lastResult > bestResult) {
			bestResult = lastResult;
		}


		mHandler.sendEmptyMessage(0);
		
		if (lastResult>GameScreen.getTodaysBestResult()){
			GameScreen.setTodaysBestResult(lastResult);
		}
		
		lastScoreText.setValue(lastResult);
		bestScoreText.setValue(GameScreen.getTodaysBestResult());
		
		GameScreen.setSavedBestResult(bestResult);
		GameScreen.setSavedLastResult(lastResult);
	}


	/**
	 * drawing thread
	 */
	@Override
	public void run() {
		// this.setPriority(MAX_PRIORITY);
		// this.setName("gamemanager");
		while (mRunning) {
			Canvas canvas = null;
			try {
				canvas = mSurfaceHolder.lockCanvas();
				synchronized (mSurfaceHolder) {
					updateObjects();
					refreshCanvas(canvas);
				}
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				if (canvas != null) {
					mSurfaceHolder.unlockCanvasAndPost(canvas);
				}
			}
		}
	}

	/**
	 * 
	 * @param canvas
	 */
	private void refreshCanvas(Canvas canvas) {
		canvas.drawColor(Color.WHITE);
		
		canvas.drawBitmap(mBitmap2, 0, 0, null);
 
		// drawing objects
		myRect.draw(canvas);
		enemies[0].draw(canvas);
		enemies[1].draw(canvas);
		enemies[2].draw(canvas);
		enemies[3].draw(canvas);
		
		canvas.drawText(lastScoreText, 0, lastScoreText.length(),
				mScreenWidthLeftText, 12, mScorePaint);
		canvas.drawText(bestScoreText, 0, bestScoreText.length(),
				mScreenWidthRightText, 12, mScorePaint);
	}

	/**
	 * Updates game objects
	 */
	private void updateObjects() {
		// update our object
		myRect.update();

		// update enemies
		movenemy(0);
		movenemy(1);
		movenemy(2);
		movenemy(3);

		// intersection with enemies and walls
		if (myRect.intersects(enemies[0].getRect())
				|| myRect.intersects(enemies[1].getRect())
				|| myRect.intersects(enemies[2].getRect())
				|| myRect.intersects(enemies[3].getRect())
				|| myRect.getLeft() < 0 || myRect.getRight() > mScreenWidth
				|| myRect.getTop() < 0 || myRect.getBottom() > mScreenHeight) {

			GameView.setDraggingStatus(0);

			speedHandler.removeCallbacks(runnable);
			stopTimer();
			setResult();
			speedByTime = 1;
			speed[0] = 0;
			speed[1] = 0;
			speed[2] = 0;
			speed[3] = 0;
			initPositions();
		}
	}

	private void movenemy(int num) {
		if (enemies[num].getRight() >= mScreenWidth
				|| enemies[num].getLeft() <= 0) {
			enemyXdir[num] = -1 * enemyXdir[num];
		}
		if (enemies[num].getBottom() >= mScreenHeight
				|| enemies[num].getTop() <= 0) {
			enemyYdir[num] = -1 * enemyYdir[num];
		}

		enemies[num].setX(enemies[num].getX()
				+ (speed[num] * enemyXdir[num] * speedByTime));
		enemies[num].setY(enemies[num].getY()
				+ (speed[num] * enemyYdir[num] * speedByTime));

		enemies[num].update();
	}

	private int rand(int number) {
		return (int) Math.ceil(Math.random() * number);
	}

	private int rt() {
		return rand(3) - 2;
	}

	private int rx() {
		tmp = rt();
		while (tmp == 0) {
			tmp = rt();
		}
		return tmp;
	}

	final class ScoreBar implements CharSequence {

		private final int prefixLen;
		private final StringBuilder buf;
		private float value;

		public ScoreBar(String prefix) {
			this.prefixLen = prefix.length();
			this.buf = new StringBuilder(prefix);
		}

		private boolean hasValue() {
			return buf.length() > prefixLen;
		}

		public void setValue(float value) {
			if (hasValue() && this.value == value)
				return;
			this.value = value;
			buf.setLength(prefixLen);
			buf.append(value);
		}

		@Override
		public char charAt(int index) {
			return buf.charAt(index);
		}

		@Override
		public CharSequence subSequence(int start, int end) {
			return buf.subSequence(start, end);
		}

		@Override
		public int length() {
			return buf.length();
		}

		@Override
		public String toString() {
			return buf.toString();
		}
	}
}
