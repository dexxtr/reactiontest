package com.zigles.reactiontest;

import android.app.Activity;
import android.os.Bundle;
import android.provider.Settings.Secure;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.flurry.android.FlurryAgent;

public class Credits extends Activity implements OnClickListener {
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.credits);
		
		TextView tv = (TextView) findViewById(R.id.credits_content);
		tv.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		finish();
	}
	
	public void onStart() {
 	   super.onStart();
 	   FlurryAgent.onStartSession(this, "D1ZEQSPTRRIVYPZGPJUQ");
 	   
 	   String uid = Secure.getString(this.getContentResolver(), Secure.ANDROID_ID);
	   FlurryAgent.setUserId(uid);
	   
	   FlurryAgent.onPageView();
 	}
 	
 	public void onStop() {
 	   super.onStop();
 	   FlurryAgent.onEndSession(this);
 	}
}